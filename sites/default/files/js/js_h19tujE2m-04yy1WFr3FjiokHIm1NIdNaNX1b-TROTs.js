(function ($) {
//alert('wtf');

  /**
   * The recommended way for producing HTML markup through JavaScript is to write
   * theming functions. These are similiar to the theming functions that you might
   * know from 'phptemplate' (the default PHP templating engine used by most
   * Drupal themes including Omega). JavaScript theme functions accept arguments
   * and can be overriden by sub-themes.
   *
   * In most cases, there is no good reason to NOT wrap your markup producing
   * JavaScript in a theme function.
   */
  Drupal.theme.prototype.lovetribevibesExampleButton = function (path, title) {
    // Create an anchor element with jQuery.
    return $('<a href="' + path + '" title="' + title + '">' + title + '</a>');
  };

    

  /**
   * Behaviors are Drupal's way of applying JavaScript to a page. In short, the
   * advantage of Behaviors over a simple 'document.ready()' lies in how it
   * interacts with content loaded through Ajax. Opposed to the
   * 'document.ready()' event which is only fired once when the page is
   * initially loaded, behaviors get re-executed whenever something is added to
   * the page through Ajax.
   *
   * You can attach as many behaviors as you wish. In fact, instead of overloading
   * a single behavior with multiple, completely unrelated tasks you should create
   * a separate behavior for every separate task.
   *
   * In most cases, there is no good reason to NOT wrap your JavaScript code in a
   * behavior.
   *
   * @param context
   *   The context for which the behavior is being executed. This is either the
   *   full page or a piece of HTML that was just added through Ajax.
   * @param settings
   *   An array of settings (added through drupal_add_js()). Instead of accessing
   *   Drupal.settings directly you should use this because of potential
   *   modifications made by the Ajax callback that also produced 'context'.
   */
  Drupal.behaviors.lovetribevibesExampleBehavior = {
    attach: function (context, settings) {
      // By using the 'context' variable we make sure that our code only runs on
      // the relevant HTML. Furthermore, by using jQuery.once() we make sure that
      // we don't run the same piece of code for an HTML snippet that we already
      // processed previously. By using .once('foo') all processed elements will
      // get tagged with a 'foo-processed' class, causing all future invocations
      // of this behavior to ignore them.
      $('.some-selector', context).once('foo', function () {
        // Now, we are invoking the previously declared theme function using two
        // settings as arguments.
        var $anchor = Drupal.theme('lovetribevibesExampleButton', settings.myExampleLinkPath, settings.myExampleLinkTitle);

        // The anchor is then appended to the current element.
        $anchor.appendTo(this);
      });
    }
  };

})(jQuery);
;


 jQuery(document).ready(function() {

  /* window size monitor */
  jQuery.windowWidth = jQuery(window).width();
	jQuery.windowHeight = jQuery(window).height();
	jQuery('#widthMonitor').html(jQuery.windowWidth);  //same logic that you use in the resize...

  jQuery(window).resize(function() {
  jQuery.windowWidth = jQuery(window).width();
	jQuery.windowHeight = jQuery(window).height();
	jQuery('#widthMonitor').html(jQuery.windowWidth);
                      });

  /*MAIN NAV DROPDOWN */
  jQuery( '.dropdown' ).hover(
        function(){
          //alert('hover');
            jQuery(this).children('.sub-menu').css('opacity', 0);
            jQuery(this).children('.sub-menu').fadeIn(200);
            jQuery(this).children('.sub-menu').slideDown(200);
            jQuery(this).children('.sub-menu').animate({opacity:1}, {queue:false, duration:300}); 
        },
        function(){
            jQuery(this).children('.sub-menu').slideUp(200);
        }
    );
  
  var mobileNavOn = false;

	jQuery('.icon-menu').mousedown( function(){
    	jQuery('#mobile-nav').slideDown();
      jQuery('form-text').focusin();
        mobileNavOn = true;

	   }); 
    jQuery('.mobile-back-btn').click( function(){
    	jQuery('#mobile-nav').slideUp();
        //alert(mobileNavOn);
			});
    
    jQuery('#mobile-nav').mouseleave(function(){
      if(mobileNavOn == true){
        jQuery(this).slideUp();
        mobileNavOn = false;
      }
    })

    
/* THIS CAUSES MENU TO SLIDE BACK UP IMMEDIATELY

    jQuery('#mobile-nav').bind('click', function(){
        stopPropogation();
    });
    jQuery(document).bind('click', function(e){
        if((!jQuery(e.target).is('#mobile-nav')) && (!jQuery(e.target).is('.icon-menu')))  {
            jQuery('#mobile-nav').slideUp();        
        } 
    }); 

 */   

/*    jQuery('#mobile-nav').click(function(e){e.stopPropogation();});
    jQuery(window).click(function(e){ jQuery('#mobile-nav').slideUp();});
*/
    /* jQuery(window).click(function(){jQuery('#mobile-nav').slideUp();});  */

  /*SEARCH BOX STUFF */
  /* jQuery('#edit-submit--27').hover(function(){ */
  jQuery('#sb-search .form-submit').hover(function(){  
        
        jQuery('.form-item-search-block-form input[type="text"]').css('border-left', '1px solid white'); 
        
        //jQuery('#edit-search-block-form--8').animate({left:0, width:'135px'});  
      
      
        if((jQuery.windowWidth == 1025) || (jQuery.windowWidth > 1025)){
          jQuery('.form-item-search-block-form input[type="text"]').animate({left:0, width:'105px'});
        } else {
          
          jQuery('.form-item-search-block-form input[type="text"]').animate({left:0, width:'60px'}); 
        }  
      
    });


  jQuery('li.search .form-submit').hover( function(){ /* mobile NOT FUCKING WORKING */
        //alert('hovered on');  
        //jQuery('#edit-search-block-form--8').toggle('slide');
        //jQuery('#edit-search-block-form--8').fadeIn();  
        //jQuery('#edit-search-block-form--8').animate({width:'80%'});
        //jQuery('input.form-text').animate({width:'80%'});
 
  });
  jQuery('#edit-search-block-form--8').focus( function(){
        
        jQuery(this).val(' '); 
  })

  jQuery('.join-us-homepage-footer').focus( function(){
        
        jQuery(this).val(''); 
        jQuery(this).attr("placeholder", "");
        return;
  })

/* BROWSER ALBUMS REDIRECT TO MOBILE IF... */
   
/* if((window.location.href.indexOf("browse-albums") > -1) && (jQuery.windowWidth <= 580)) { 
       alert("doesnt contains glowhold");
    }
*/

  

}); ;
