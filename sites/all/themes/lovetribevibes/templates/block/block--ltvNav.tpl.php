<?php

/**
 * @file
 * Minimal theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In 
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php

?>




<nav<?php print $attributes; ?>>
<div id="top-nav">
		<ul>
			<?php if(user_is_logged_in()) { ?>
				<li><a href="/user">My Account</a></li><li><a href="/user/logout">Log Out</a></li>
				<?php } else { ?>
				<li><a href="/user/login">Log In</a></li>
				<li><a href="/user/register">Create Account</a></li>
				<?php	} ?>
		<li><a href="/contact">Contact</a></li>
		</ul>
</div>
<div id="main-nav-wrapper">
	<div id="main-nav-inner-wrapper">
		<ul id="ltv-main-nav" class="main-nav-hc">
			<li class="home-logo"><a href="/"><img src="/sites/default/files/menu_logo_green.png" id="home-logo" alt="Love Tribe Vibes logo"></a></li>
			<li class="home-icon"><a href="/"><img src="/sites/default/files/home-icon-trans.png" height="22" alt="home icon"></a></li>
			<li class="sep txt"><a href="/mission">Mission</a></li>
			<li class="shop-music sep txt"><a href="/browse-albums">Music</a></li>

			<li class="shop-dvds sep txt"><a href="/yoga">Yoga Streams</a></li>
			<li class="sep txt no-anchor dropdown"><span>Shop</span>
				<ul class="sub-menu">
					<li><a href="/dvds">DVD's</a></li>
					<li><a href="/books/lets-love-and-be-real">Books</a></li>
				</ul>
			</li> 
			<li span class="cart"><a href="/cart" class="flaticon-shopping-cart13"></a></li> 

			<?php  $ajaxCart = module_invoke('views', 'block_view', 'shopping_cart-block');?>
			<li id="ajax-shopping-cart-wrap"><?php  $ajaxCart = module_invoke('views', 'block_view', 'shopping_cart-block');print render($ajaxCart['content'])?></li>	
			<li id="search-open"><a href="#" class="flaticon-magnifier13"></a></li>
		</ul> 
		<!--
		<div id="sb-search" class="sb-search">
			<?php print $search_box;  ?>
		</div>
	-->
	<div id="search-container"><?php print $search_box;  ?></div> 
	</div>

</div>
<div id="mobile-nav-wrapper">
	<ul id="mobile-header">
		<li class="home-logo"><a href="/"><img src="/sites/default/files/menu_logo_green.png"></a></li>
		<li class="icon-menu"></li>
	</ul>
</div>
<ul id="mobile-nav">
	<?php if(user_is_logged_in()) { ?>
		<li><a href="/user/logout">Logout</a></li>
		<li><a href="/user">My Account</a></li>
	<?php } else { ?>
		<li><a href="/user/login">Login</a></li>
		<li><a href="/user/register">Create Account</a></li>
	<?php } ?>
	<li><a href="/mission">Mission</a></li>
	<li><a href="/dvds">DVDs</a></li>
	<li><a href="/browse-albums-mobile">Music</a></li>
	<li><a href="/yoga">Yoga Classes</a></li> 
	<li><a href="/books/lets-love-and-be-real">Books</a></li> 
	<li><a href="/cart">Cart</a></li>
	

	<li><a href="/contact">Contact</a></li>
	<!-- <li><a href="#" class="flaticon-magnifier13"> </a></li> -->
	<li class="search"><?php print $search_box; ?></li>
	<li class="mobile-back-btn"><a href="#">&lt; Back</a></li>
</ul>
</nav> 