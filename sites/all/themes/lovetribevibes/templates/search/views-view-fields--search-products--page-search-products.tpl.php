<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

	$nid = strip_tags($fields['nid']->content);
	$title = $fields['title']->content;
	$price = $fields['commerce_price']->content;
	$add_to_cart = $fields['add_to_cart_form']->content;

	// field_album_cover_front_file
	$image = '';
	$ac = strip_tags($fields['field_product_field_album_cover_front_file']->content);
	if ($ac) {
		$image = strip_tags($fields['field_product_field_album_cover_front_file']->content);
	}

	// field_class_pass_image
	$ci = strip_tags($fields['field_product_field_class_pass_image_file']->content);
	if ($ci) {
		$image = strip_tags($fields['field_product_field_class_pass_image_file']->content);
	}

	// field_book_cover_file
	$bc = strip_tags($fields['field_product_field_book_cover_file']->content);
	if ($bc) {
		$image = strip_tags($fields['field_product_field_book_cover_file']->content);
	}

	// field_dvd_cover_file
	$dc = strip_tags($fields['field_product_field_dvd_cover_file']->content);
	if ($dc) {
		$image = strip_tags($fields['field_product_field_dvd_cover_file']->content);
	}

	// field_images
	$field_images = '';
	$fi = strip_tags($fields['field_images']->content);
	if ($fi) {
		$field_images = strip_tags($fields['field_images']->content);
	}

	// field_track_thumb_file
	$tt = strip_tags($fields['field_product_field_track_thumb_file']->content);
	if ($tt) {
		$image = strip_tags($fields['field_product_field_track_thumb_file']->content);
	}

	$file = '';
	if (isset($image)) {
		$file = file_load($image);
	}

	$ltv_track = strip_tags($fields['field_track_sample_rev']->content);
	$type = strip_tags($fields['type']->content);
	$artist = strip_tags($fields['field_artist_ref']->content);
	$mnetid = strip_tags($fields['field_mnetid']->content);
	$mne_artist = strip_tags($fields['mnmedia_album_artist']->content);

?>

<div class="result-item">
	<div class="result-image">
		<?php if (isset($field_images)): ?>
			<?php print $fields['field_images']->content; ?>
		<?php endif; ?>
		<?php if (!empty($file)): ?>
			<?php print l(theme_image(array('path' => $file->uri, 'getsize' => TRUE,
				'attributes' =>
				array('class' => 'thumb', 'width' => '160', 'height' => '160'))),
				'node/'. $nid, array('html' => TRUE)); ?>
		<?php endif; ?>
	</div>
	<?php if (!empty($ltv_track)): ?>
		<div class="result-ltv-track">
			<?php print $fields['field_track_sample_rev']->content; ?>
		</div>
	<?php endif; ?>
	<div class="result-title">
		<?php print $title; ?>
	</div>
	<?php if (!empty($mnetid)): ?>
		<div class="result-mnetid">
			<?php print $fields['field_mnetid']->content; ?>
		</div>
	<?php endif; ?>
	<div class="result-type">
		<?php print $type; ?>
	</div>
	<?php if (!empty($artist)): ?>
		<div class="result-artist">
			<?php print $fields['field_artist_ref']->content; ?>
		</div>
	<?php endif; ?>
	<?php if (!empty($mne_artist)): ?>
		<div class="result-artist">
			<?php print $fields['mnmedia_album_artist']->content; ?>
		</div>
	<?php endif; ?>
	<div class="result-price">
		<?php print $price; ?>
	</div>
	<div class="result-add-to-cart">
		<?php print $add_to_cart; ?>
	</div>
</div>

