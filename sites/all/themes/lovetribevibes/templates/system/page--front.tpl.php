
<div class="l-page">
  <header class="l-header" role="banner">
    

    <?php print render($page['header']); ?>
    <?php print render($page['topnav']); ?>
    <?php print render($page['navigation']); ?>
  </header>

  <div class="l-main">
    <div class="l-content" role="main">
      <?php print render($page['highlighted']); ?>
      <!-- <?php print $breadcrumb; ?> -->
      
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if($is_admin): ?>
      <?php print $messages; ?>
      <?php endif; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
  </div><!-- end main -->

</div>

<?php //if ($super_footer): doesn't work! ?>
<div class="super-footer" role="contentinfo">
    <?php print render($page['super_footer']); ?>
</div>
<?php //endif; ?> 
<footer>
<div class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
</div> 
</footer>
<!-- NEED TO ADD COMMMENTS RE: WHICH FILES ARE FOR SLOGAN ANIM 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="/sites/all/themes/lovetribevibes/js/assets/jquery.fittext.js"></script>
<script src="/sites/all/themes/lovetribevibes/js/assets/jquery.lettering.js"></script>
<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<script src="/sites/all/themes/lovetribevibes/js/jquery.textillate.js"></script> -->
<script src="http://lovetribevibes.com/profiles/commerce_kickstart/modules/contrib/service_links/js/twitter_button.js?nx1rtc"></script>
<script src="http://lovetribevibes.com/profiles/commerce_kickstart/modules/contrib/service_links/js/facebook_like.js?nx1rtc"></script>
<script src="/sites/all/themes/lovetribevibes/js/twitter.js"></script>





