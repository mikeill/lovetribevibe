name = Two-Footer
description = A Layout with 2 Footers
preview = preview.png
template = two-footer-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[footer_1]       = Footer1
regions[footer_2]       = Footer2

; Stylesheets
stylesheets[all][] = css/layouts/two-footer/two-footer.layout.css
