name = Raspberry
description = description goes here
template = raspberry-layout

;Regions
regions[branding] = Branding
regions[header] = Header
regions[navigation] = Navigation
regions[highlighted] = Highlighted
regions[help] = Help
regions[content] = Content
regions[sidebar_first] = First Sidebar
regions[footer] = Footer

;Styles
stylesheets[all][] = css/layouts/raspberry/raspberry.layout.css
stylesheets[all][] = css/layouts/raspberry/raspberry.layout.no-query.css
stylesheets[all][] = css/layouts/raspberry/ltv-raspberry.layout.css 