<?php

/**
*
* @file Admin settings page.
*/

function commerce_payment_confirm_settings_form($form,&$form_state) {
  $form = array();
  $form['commerce_payment_confirm_message'] = array(
    '#title' => t('Confirmation Dialog Message'),
    '#description' => t('Enter the text you would like to show on the confirmation dialog here.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('commerce_payment_confirm_message', ''),
  );
  
  return system_settings_form($form);
}