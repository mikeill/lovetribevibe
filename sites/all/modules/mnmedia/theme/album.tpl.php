<?php foreach ($albums as $mnetid => $album):?>
	<div class="mnedia-album">
		<?php print theme('image', array('path' => $album->album_art_150));?>
		<div class="mnmedia-album-title"><strong>Title: </strong><?php print $album->title;?></div>
		<div class="mnmedia-album-genre"><strong>Genre: </strong><?php print $album->genre;?></div>
		<div class="mnmedia-album-artist"><strong>Artist: </strong><?php print getArtistName($album->artist_id);?></div>
		<div class="mnmedia-album-label"><strong>Label: </strong><?php print $album->label;?></div>
		<div class="mnmedia-album-duration"><strong>Duration: </strong><?php print $album->duration;?></div>
		<div class="mnmedia-album-currency"><strong>Price: </strong><?php print $album->price . ' ' .$album->currency; ?></div>
		<div class="mnmedia-album-tracks">
			<div class="track-title"><strong>Tracks: </strong></div>
			<?php print theme('tracks', array('tracks' => getTracks($mnetid)));?>
		</div>
	</div>
<?php endforeach;?>