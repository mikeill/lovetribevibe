<?php if(!isset($track['uri'])) return;?>
<div class="mnmedia_sample">
	<div class="player-wrapper">
		<div id="player-<?php print $track['fid'];?>" class="player"></div>
	</div>
	<?php
		$url = file_create_url($track['uri']);
		$url = parse_url($url);
		$path = $url['path'];
	?>
	<span class="play_pause" data-location="<?php print $path;?>" data-mp3="true" data-mnetid="<?php print $track['fid'];?>"></span>		
</div>