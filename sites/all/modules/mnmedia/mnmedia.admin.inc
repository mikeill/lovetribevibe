<?php

function mnmedia_admin_form($form, &$form_state) {
	$form = array();
	$form['mnmedia'] = array(
		'#type' => 'vertical_tabs',
	);

	$form['mnmedia']['mn_basic'] = array(
		'#type' => 'fieldset',
		'#title' => t('Basic Settings'),
		'#group' => 'basic',
	);
	
	$form['mnmedia']['mn_basic']['mnmedia_environment'] = array(
		'#type' => 'select',
		'#title' => t('Use Environment'),
		'#options' => array(
			'production' => t('Production'),
			'integration' => t('Integration')
		),
		'#description' => t('Choose which Environment to run.'),
		'#default_value' => variable_get('mnmedia_environment', 'integration'),
	);
	
	$form['mnmedia']['mn_basic']['include_explicit'] = array(
		'#type' => 'checkbox',
		'#title' => t('Include Explicit?'),
		'#description' => t('Include Explicit when pulling feed.'),
		'#default_value' => variable_get('include_explicit', FALSE),
	);
	
	$form['mnmedia']['mn_production'] = array (
		'#type' => 'fieldset',
		'#title' => t('Production Settings'),
		'#group' => 'production',
		'#description' => t('Setup Production Environment...'),
	);

	$form['mnmedia']['mn_production']['mn_production_api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('API Key'),
		'#default_value' => variable_get('mn_production_api_key', ''),
		'#description' => t('API Key'),
	);

	$form['mnmedia']['mn_production']['mn_production_shared_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Shared Secret'),
		'#default_value' => variable_get('mn_production_shared_secret', ''),
		'#description' => t('Shared Secret'),
	);

	$form['mnmedia']['mn_integration'] = array(
		'#type' => 'fieldset',
		'#title' => t('Integration Settings'),
		'#group' => 'integration',
		'#description' => t('Setup Integration Environment'),
	);

	$form['mnmedia']['mn_integration']['mn_integration_api_key'] = array(
		'#type' => 'textfield',
		'#title' => t('API Key'),
		'#default_value' => variable_get('mn_integration_api_key', ''),
		'#description' => t('API Key'),
	);

	$form['mnmedia']['mn_integration']['mn_integration_shared_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Shared Secret'),
		'#default_value' => variable_get('mn_integration_shared_secret', ''),
		'#description' => t('Shared Secret'),
	);

	return system_settings_form($form);
}

function mnmedia_search_form($form, &$form_state) {
	$form['search'] = array(
		'#type' => 'fieldset',
		'#title' => t('Search'),
		'#weight' => 5,
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
		// '#attributes' => array('class' => array('exposed-filters')),
		'#weight' => -10,
	);
	$form['search']['title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title '),
		// '#description' => t('Title'),
		'#size' => 30,
	);
	$form['search']['artist'] = array(
		'#type' => 'textfield',
		'#title' => t('Artist '),
		// '#description' => t('Artist'),
		'#size' => 30,
	);
	$form['search']['artistMnetId'] = array(
		'#type' => 'textfield',
		'#title' => t('artistMnetId '),
		// '#description' => t('artistMnetId'),
		'#size' => 30,
	);
	$form['search']['submit'] = array(
		'#type' => 'submit', 
		'#value' => t('Search')
	);	
	
	$header = array(
		'title' => array('data' => array('#title' => 'Title')),
		'artist' => t('Artist'),
		'album_art' => t('Album Art'),
		'tracks' => t('Tracks'),
		'price' => t('Price'),
	);

	$options = array();
	$albums = false;
	if(isset($form_state['api_data'])):
		$albums = $form_state['api_data'];
	elseif (isset($_COOKIE['Drupal_visitor_mnmedia_search'])):
		$data = mnmedia_getApiData();
		if ($data) {
			$albums = $data->Albums;
			$form_state['api_data'] = $albums;
		}
	endif;
	if ($albums) {
		foreach ($albums as $album) {
			$albumTitle = isset($album->Title) ? $album->Title : 'UNKNOWN';
			$artistName = isset($album->Artist->Name) ? $album->Artist->Name : 'UNKNOWN';
			$tracks = isset($album->Tracks) ? $album->Tracks : array();
			$amount = isset($album->PriceTag->Amount) ? $album->PriceTag->Amount : '';
			$currency = isset($album->PriceTag->Currency) ? $album->PriceTag->Currency : '';
			$options[$album->MnetId] = array(
				'title' => $albumTitle,
				'artist' => $artistName,
				'album_art' => theme('image', array('path' => $album->Images->Album150x150)),
				'tracks' => theme('tracks', array('tracks' => $tracks)),
				'price' => $amount . ' ' . $currency,
			);
		}
		$form['action'] = array(
			'#type' => 'fieldset',
			'#title' => t('Action'),
			'#weight' => 5,
			'#collapsible' => FALSE,
			'#collapsed' => FALSE,
			// '#attributes' => array('class' => array('exposed-filters')),
			'#weight' => 10,
		);
		$form['action']['save'] = array(
			'#type' => 'submit', 
			'#value' => t('Save Selected'),
			'#submit' => array('mnmedia_search_form_save')
		);
	}

	$form['albums'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No results in the catalog'),
		'#weight' => 20,
	);

	$per_page = 5;
	$total_page = 0;
	if (isset($form_state['api_TotalResults'])) {
		$total_page = $form_state['api_TotalResults'];
	} elseif (isset($data->TotalResults)) {
		$total_page = $data->TotalResults;
	}
	$current_page = pager_default_initialize($total_page, $per_page);

	$form['pager'] = array(
		'#markup' => theme('pager', array('quantity', $total_page)),
		'#weight' => 30,
	);
	return $form;
}

function mnmedia_search_form_validate ($form, &$form_state) {	
	if (!$form_state['values']['title'] && !$form_state['values']['artist'] && !$form_state['values']['artistMnetId']) {
		form_set_error(NULL, 'Please fill at least one.');
	}
}

function mnmedia_search_form_submit ($form, &$form_state) {
	$title = $form_state['values']['title'];
	$artist = $form_state['values']['artist'];
	$artistMnetId = $form_state['values']['artistMnetId'];
	$options = array(
		'method' => 'GetAlbums',
		'action' => 'Search',
		'title' => $title, 
		'artist' => $artist,
		'artistMnetId' => $artistMnetId,
		'page' => 1,
		'pageSize' => 5,
	);
	$data = mnmedia_getApiData($options);
	user_cookie_save(array('mnmedia_search' => serialize($options)));
	if (!isset($data) || $data->Success != TRUE) {
		form_set_error(NULL, 'Could not make the API. Please try again or contact the module developer.');
	} else {
		$form_state['next_step'] = TRUE;
		$form_state['api_data'] = $data->Albums;
		$form_state['api_TotalResults'] = $data->TotalResults;
  		$form_state['rebuild'] = TRUE;
	}
}

function mnmedia_catalog_form_update ($form, &$form_state) {
	if (!isset($form_state['input']['albums'])) {
		form_set_error(NULL, 'No Albums were deleted. To delete an album, choose below and try again.');
		return;
	}
	$albums = $form_state['input']['albums'];
	$delete = FALSE;
	foreach ($albums as $key => $value) {
		if ($value !== NULL) {
			mnmedia_album_delete($key);
			$delete = TRUE;
		}
	}
	if (!$delete) {
		form_set_error(NULL, 'No Albums were deleted. To delete an album, choose below and try again.');
		return;		
	}
}

function mnmedia_search_form_save ($form, &$form_state) {
	if (!isset($form_state['input']['albums'])) {
		form_set_error(NULL, 'No Albums were saved. You need to choose albums by checking the table below.');
		return;
	}
	// dsm($form_state);
	foreach ($form_state['input']['albums'] as $mnetid => $mnetidExists) {
		if ($mnetidExists) {
			foreach ($form_state['api_data'] as $album) {
				if ($album->MnetId == $mnetid) {
					mnmedia_artist_save (
						array (
							'mnetid' => $album->Artist->MnetId,
							'name' => $album->Artist->Name,
							'artist_image_180' => $album->Artist->Images->Artist180x80,
							'artist_image_190' => $album->Artist->Images->Artist190x230,
							'artist_image_375' => $album->Artist->Images->Artist375x250,
						)
					);
					mnmedia_album_save (
						array (
							'mnetid' => $album->MnetId,
							'artist_id' => $album->Artist->MnetId,
							// 'artist_name' => $album->Artist->MnetId,
							'title' => $album->Title,
							'genre' => $album->Genre,
							'label' => $album->Label,
							'duration' => $album->Duration,
							'album_art_75' => $album->Images->Album75x75,
							'album_art_150' => $album->Images->Album150x150,
							'album_art_800' => $album->Images->Album800x800,
							'price' => $album->PriceTag->Amount,
							'currency' => $album->PriceTag->Currency,
						)
					);
					foreach ($album->Tracks as $trackRecord) {
						$trackOptions = array(
							'method' => 'Get',
							'action' => 'Track',
							'MnetId' => $trackRecord->MnetId,
						);
						$trackinfo = mnmedia_getApiData($trackOptions);
						if ($trackinfo->Success != TRUE) break;
						$track = $trackinfo->Track;
						$sampleLocation = '';
						$sampleResource = '';
						foreach ($track->SampleLocations as $track_location) {
							if ($track_location->Type == 's_mp3') {
								$sampleLocation = $track_location->Location;
								$sampleResource = $track_location->Resource;
							}
						}
						mnmedia_track_save (
							array (
								'mnetid' => $track->MnetId,
								'artist_id' => $track->Artist->MnetId,
								'album_id' => $album->MnetId,
								'title' => $track->Title,
								'genre' => $track->Genre,
								'duration' => $track->Duration,
								'track_number' => $track->TrackNumber,
								'can_sample' => $track->Mp3Rights->CanSampleStream,
								'sample_location' => $sampleLocation,
								'sample_resource' => $sampleResource,
								'price' => isset($track->PriceTag->Amount) ? $track->PriceTag->Amount : 0,
								'currency' => isset($track->PriceTag->Currency) ? $track->PriceTag->Currency : '',
							), $track->Mp3Rights->CanPurchaseDownload
						);
					}

					break;
				}
			}
		}
	}
}

function mnmedia_album_delete ($mnetid) {
	mnmedia_tracks_delete($mnetid);
	$album_deleted = db_delete('mnmedia_albums')
	  ->condition('mnetid', $mnetid)
	  ->execute();
	drupal_set_message($album_deleted . ' Albums deleted with Album MnetId = ' . $mnetid);
}

function mnmedia_tracks_delete ($mnetid) {
	$tracks_deleted = db_delete('mnmedia_tracks')
	  ->condition('album_id', $mnetid)
	  ->execute();
	drupal_set_message($tracks_deleted . ' Tracks deleted with Album MnetId = ' . $mnetid);
}

function mnmedia_album_save($album) {
  db_merge('mnmedia_albums')
    ->key(array('mnetid' => $album['mnetid']))
    ->fields($album)
    ->execute();

	if (module_exists('mnmedia_commerce')) {
		rules_invoke_event('mnmedia_album_save_event', $album['mnetid']);
	}
  	// drupal_set_message ('Album <strong>' . $album['title'] . '</strong> has been updated in the local catalog');
}

function mnmedia_artist_save($artist) { 
  db_merge('mnmedia_artists')
    ->key(array('mnetid' => $artist['mnetid']))
    ->fields($artist)
    ->execute();

	if (module_exists('mnmedia_commerce')) {
		rules_invoke_event('mnmedia_artist_save_event', $artist['mnetid']);
	}
}

function mnmedia_track_save($track, $CanPurchaseDownload) {
  db_merge('mnmedia_tracks')
    ->key(array('mnetid' => $track['mnetid']))
    ->fields($track)
    ->execute();
    if (module_exists('mnmedia_commerce') && $CanPurchaseDownload) {
    	drupal_set_message('Track can be saved');
		rules_invoke_event('mnmedia_track_save_event', $track['mnetid']);
	} else {
		drupal_set_message('Track <strong>' . $track['title'] . '</strong> cannot be sold separately.');
	}
  // drupal_set_message ('Track ' . $track['title'] . ' has been updated in the local catalog');
}

function mnmedia_catalog_form ($form, &$form_state) {
	$form['action'] = array(
		'#type' => 'fieldset',
		'#title' => t('Update options'),
		'#weight' => 5,
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
		// '#attributes' => array('class' => array('exposed-filters')),
		'#weight' => 10,
	);
	$form['action']['action'] = array(
		'#type' => 'select',
		'#title' => '',
		'#options' => array(
			'delete' => t('Delete'),
		),
		'#default_value' => 'delete',
	);
	$form['action']['update'] = array(
		'#type' => 'submit', 
		'#value' => t('Update'),
		'#submit' => array('mnmedia_catalog_form_update')
	);	

	$header = array(
		'title' => array('data' => array('#title' => 'Title')),
		'artist' => t('Artist'),
		'album_art' => t('Album Art'),
		'tracks' => t('Tracks'),
		'price' => t('Price'),
	);

	$albums = false;
	$options = array();
	$result = db_query('SELECT m.mnetid, a.name, m.title, m.duration, m.album_art_150, m.price, m.currency FROM {mnmedia_albums} AS m LEFT JOIN {mnmedia_artists} AS a ON m.artist_id = a.mnetid');
	$albums = $result;

	if ($albums) {
		foreach ($albums as $album) {			
			$albumTitle = isset($album->title) ? $album->title : 'UNKNOWN';
			$artistName = isset($album->name) ? $album->name : 'UNKNOWN';
			$tracks = mnmedia_get_tracks($album->mnetid);
			$amount = isset($album->price) ? $album->price : '';
			$currency = isset($album->currency) ? $album->currency : '';
			$options[$album->mnetid] = array(
				'title' => $albumTitle,
				'artist' => $artistName,
				'album_art' => theme('image', array('path' => $album->album_art_150)),
				'tracks' => theme('tracks', array('tracks' => $tracks)),
				'price' => $amount . ' ' . $currency,
			);
		}
	}

	$form['albums'] = array(
		'#type' => 'tableselect',
		'#header' => $header,
		'#options' => $options,
		'#empty' => t('No results in the catalog'),
		'#weight' => 20,
	);

	return $form;
}

function mnmedia_get_tracks($mnetid) {
	$result = db_query('SELECT t.title AS Title, t.duration AS Duration FROM {mnmedia_tracks} AS t WHERE t.album_id = :mnetid', array(':mnetid' => $mnetid));
	return $result;
}