<?php

/**
 * MNMedia API class
 * API Documentation: 
 * Class Documentation: 
 * 
 * @author Sam Timalsina
 * @since 30.10.2011
 * @copyright Sam Timalsina - sam.timalsina@gmail.com
 * @version 1.0
 */
class MNMedia {

  /**
   * The API base URL
   */
  
  /**
   * The MNMedia API Key
   * 
   * @var string
   */
  private $_apikey;

  /**
   * The MNMedia OAuth API secret
   * 
   * @var string
   */
  private $_apisecret;
  private $apiurl;

  /**
   * Available scopes
   * 
   * @var array
   */
  private $_defaults = array(
    'method' => 'Search.GetAlbums',
    'format' => 'json',
    'includeExplicit' => 'False',
    'mainArtistOnly' => 'False',
    'pageSize' => '5',
    'mostPopularTrackOnly' => 'False',
    'suppressTrackInfo' => 'False',
    'includeKaraoke' => 'False',
  );

  /**
   * Default constructor
   *
   * @param array|string $config MNMedia configuration data
   * @return void
   */
  public function __construct($config) {
    if (true === is_array($config)) {
      $this->setApiKey($config['apiKey']);
      $this->setApiSecret($config['apiSecret']);
      $this->setApiEnvironment($config['environment']);
    } else {
      throw new Exception("Error: __construct() - Configuration data is missing.");
    }
  }

   public function request ($options = array()) {
    $action = $options['action'];
    $method = $options['method'];
    unset($options['action']);
    unset($options['method']);
    $url = $this->apiurl . "?method=" . $action . "." . $method;
    $url .= "&format=json";
    foreach($options as $key=> $option) {
      $url .= '&' . $key . '=' . urlencode($option);
    }
    // $url .= "&rights=";
    // $url .= "&keyword=";
    // $url .= "&includeExplicit=False";
    // $url .= "&mainArtistOnly=False";
    // $url .= "&genre=";
    // $url .= "&pageSize=5";
    // $url .= "&mostPopularTrackOnly=False";
    // $url .= "&suppressTrackInfo=False";
    // $url .= "&includeKaraoke=False";
    $url .= "&timestamp=" . time();
    $signature = $this->hashQuery($url);
    $url .= "&apiKey=" . $this->getApiKey();
    $url .= "&signature=" . $signature;
    return $this->_run($url);
  }

  public function _run($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $jsonData = curl_exec($ch);
    $data = json_decode($jsonData);
    if(isset($data->Success))
      return $data;
    else {
      drupal_set_message('There was a problem with the API call. Please contact the developer of this module.', 'error');
      return false;
    }
  }

  public function cartRequest($options) {
    $url = "method=" . $options['method'];
    $url .= "&format=json";
    if (isset($options['CartId'])) {
      $url .= "&cartId=" . $options['CartId'];
    }
    $url .= "&amp;timestamp=" . time();
    $url .= "&apiKey=" . $this->getApiKey();
    if(isset($options['OrderId'])) {
      $url .= "&orderid=" . $options['OrderId'];
    }
    if (isset($options['tracking'])) {
      $url .= "&userIp=" . ip_address();
      $url .= "&userDomain=" . $GLOBALS['base_url'];
      $url .= "&username=raviana";
    }
    $signature = $this->hashQuery($url);
    $url .= "&signature=" . $signature;
    return $this->_runWithPost($this->apiurl . "?" . $url, $options['post']);
  }

  public function _runWithPost($url, $post) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $processed_post = 'None';
    if($post) {
      $processed_post = json_encode($post);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, utf8_encode($processed_post));
    }
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $jsonData = curl_exec($ch);
    if(curl_errno($ch)) {
      drupal_set_message('There was a problem with the API call. Please contact the developer of this module. Error: ' . curl_error($ch), 'error');
      watchdog('mnmedia_api', 'Curl Error: %error', array('%error' => curl_getinfo($ch)), WATCHDOG_ERROR);
      return false;
    }
    watchdog('mnmedia_api', "<strong>API Call:</strong><br><strong>URL:</strong> <pre>%url</pre><br><strong>POST:</strong> <pre>%post</pre><br><strong>RESPONSE:</strong> <pre>%response</pre>", array('%url' => $url, '%post' => $processed_post, '%response' => $jsonData), WATCHDOG_DEBUG);
    $data = json_decode($jsonData);
    if(!isset($data->Success)) {
      drupal_set_message('Item could not be added to the cart.', 'error');
      $data = false;
    }
    curl_close($ch);
    return $data;
  }

    /**
   * API-key Setter
   * 
   * @param string $apiKey
   * @return void
   */
  public function setApiKey($apiKey) {
    $this->_apikey = $apiKey;
  }

  /**
   * API Key Getter
   * 
   * @return string
   */
  public function getApiKey() {
    return $this->_apikey;
  }

  /**
   * API Secret Setter
   * 
   * @param string $apiSecret 
   * @return void
   */
  public function setApiSecret($apiSecret) {
    $this->_apisecret = $apiSecret;
  }

  /**
   * API Secret Getter
   * 
   * @return string
   */
  public function getApiSecret() {
    return $this->_apisecret;
  }

  private function hashQuery($query) {
    return hash_hmac('md5', $query, $this->getApiSecret());
  }

  private function getAction($action) {
    switch ($action) {
      case 'search': default:
        return 'Search';
        break;
    }
  }

  public function setApiEnvironment($environment) {
    $this->apiurl = ($environment == 'production') ? 'https://api.mndigital.com' : 'https://ie-api.mndigital.com';
  }

}
