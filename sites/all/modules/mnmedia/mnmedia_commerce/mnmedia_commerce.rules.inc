<?php
/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function mnmedia_commerce_rules_event_info() {
  return array(
    'mnmedia_artist_save_event' => array(
      'label' => t('An Artist is updated in the local catalog'),
      'group' => 'MN Media',
      'variables' => array(        
        'artist' => array(
          'type' => 'mnmedia_artist',
          'label' => t('Artist from the local catalog'),
        ),
      ),
    ),
    'mnmedia_album_save_event' => array(
      'label' => t('An Albumm is updated in the local catalog'),
      'group' => 'MN Media',
      'variables' => array(        
        'album' => array(
          'type' => 'mnmedia_album',
          'label' => t('Album from the local catalog'),
        ),
      ),
    ),
    'mnmedia_track_save_event' => array(
      'label' => t('A track is updated in the local catalog'),
      'group' => 'MN Media',
      'variables' => array(        
        'track' => array(
          'type' => 'mnmedia_track',
          'label' => t('Track from the local catalog'),
        ),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_action_info().
 * @ingroup rules
 */
function mnmedia_commerce_rules_action_info() {
  return array(
    'mnmedia_artist_save_action' => array(
      'label' => t('Add Artist to Taxonomy'), 
      'parameter' => array(
        'artist' => array(
          'type' => 'mnmedia_artist',
          'label' => t('Artist from the local catalog'),
        ),
      ), 
      'group' => t('MN Media'),
    ),
    'mnmedia_album_save_action' => array(
      'label' => t('Auto create Album Product'), 
      'parameter' => array(
        'album' => array(
          'type' => 'mnmedia_album',
          'label' => t('Album from the local catalog'),
        ),
      ), 
      'group' => t('MN Media'),
    ),
    'mnmedia_track_save_action' => array(
      'label' => t('Auto create Track Product'), 
      'parameter' => array(
        'track' => array(
          'type' => 'mnmedia_track',
          'label' => t('Track from the local catalog'),
        ),
      ), 
      'group' => t('MN Media'),
    ),
  );
}

function mnmedia_album_save_action($album) {
  mnmedia_commerce_genre_save_action($album->genre);
  if(!commerce_product_validate_sku_unique('ALBUM-' . $album->mnetid, '')) {
    drupal_set_message('The Album '.$album->title.' could not be added. The SKU is duplicate', 'error');
    return;
  }
  global $user;
  $product = commerce_product_new(variable_get('album_product_type', 'product'));
  $product->uid = 1;
  $product->sku = 'ALBUM-' . $album->mnetid;
  $product->title = $album->title;
  $product->language = LANGUAGE_NONE;
  
  $product->commerce_price[LANGUAGE_NONE][0] = array (
    'amount' => $album->price * 100,
    'currency_code' => $album->currency,
  );
  $external_image = file_get_contents($album->album_art_800);
  $external_image_object = file_save_data($external_image, 'public://mnmedia/' . 'album_' . $album->mnetid . '.jpeg', FILE_EXISTS_REPLACE);

  // We save the file to the root of the files directory.
  if ($external_image_object) {
    $product->field_images[LANGUAGE_NONE][0] = (array)$external_image_object;
  } else {
    drupal_set_message('The Album Art was not saved. Please make sure the public://mnmedia exists and is writable.', 'error');
  }
  commerce_product_save($product);
  // Create a Product Display
  // And set the product reference up
  $artist = taxonomy_get_term_by_name(getArtistName($album->artist_id), 'mnmedia_artist');
  $genre = taxonomy_get_term_by_name($album->genre, 'mnmedia_genre');

  $product_display = entity_metadata_wrapper('node', entity_create('node', array('type' => variable_get('album_product_node_type', 'product_display'),'uid'=>$user->uid)));
  $product_display->title = $album->title;
  $product_display->field_product = $product;
  $product_display->field_mnetid = $album->mnetid;
  $product_display->mnmedia_album_artist = $artist;
  $product_display->field_genre = $genre;
  // Save the node.
  $product_display->save();
  drupal_set_message ('Album <strong>' . $album->title . '</strong> has been updated in the local catalog');

}

function mnmedia_track_save_action($track) {
  if(!commerce_product_validate_sku_unique('TRACK-' . $track->mnetid, '')) {
    drupal_set_message('The Track '.$track->title.' could not be added. The SKU is duplicate', 'error');
    return;
  }
  global $user;
  $product = commerce_product_new(variable_get('track_product_type', 'product'));
  $product->uid = 1;
  $product->sku = 'TRACK-' . $track->mnetid;
  $product->title = $track->title;
  $product->language = LANGUAGE_NONE;
  $product->commerce_price[LANGUAGE_NONE][0] = array (
    'amount' => $track->price * 100,
    'currency_code' => $track->currency,
  );
  $album = commerce_product_load_by_sku('ALBUM-' . $track->album_id);
  //dsm($album);
  $product->field_album[LANGUAGE_NONE][0] = array (
    'target_id' => $album->product_id,
    'target_type' => $album->type,
  );
  $album_art = getAlbumartForTrack($track->album_id);
  $external_image = file_get_contents($album_art);
  $external_image_object = file_save_data($external_image, 'public://mnmedia/' . 'track_' . $track->mnetid . '.jpeg', FILE_EXISTS_REPLACE);

  // We save the file to the root of the files directory.
  if ($external_image_object) {
    $product->field_images[LANGUAGE_NONE][0] = (array)$external_image_object;
  } else {
    drupal_set_message('The Album Art was not saved. Please make sure the public://mnmedia exists and is writable.', 'error');
  }
  commerce_product_save($product);
  // Create a Product Display
  // And set the product reference up
  $product_display = entity_metadata_wrapper('node', entity_create('node', array('type' => variable_get('track_product_node_type', 'product_display'),'uid'=>$user->uid)));
  $product_display->title = $track->title;
  $product_display->field_product = $product;
  $product_display->field_mnetid = $track->mnetid;
  $artist = taxonomy_get_term_by_name(getArtistName($track->artist_id), 'mnmedia_artist');
  $product_display->mnmedia_album_artist = $artist;
  $genre = taxonomy_get_term_by_name($track->genre, 'mnmedia_genre');
  $product_display->field_genre = $genre;
  // Save the node.
  $product_display->save();
}

function mnmedia_commerce_genre_save_action($genre) {
  $vocabulary = taxonomy_vocabulary_machine_name_load('mnmedia_genre');
  $exists = taxonomy_get_term_by_name($genre, 'mnmedia_genre');
  if(!empty($exists))
    return;
  $genre_term = (object) array(
    'name' => $genre,
    'description' => '',
    'vid' => $vocabulary->vid,
  );
  taxonomy_term_save($genre_term);
}

function mnmedia_artist_save_action($artist) {
  if (!isset($artist->name))
    return;
  $vocabulary = taxonomy_vocabulary_machine_name_load('mnmedia_artist');
  $exists = taxonomy_get_term_by_name($artist->name, 'mnmedia_artist');
  if(!empty($exists))
    return;
 
  $artist_term = (object) array(
     'name' => $artist->name,
     'description' => '',
     'vid' => $vocabulary->vid,
  );
   
  taxonomy_term_save($artist_term);
}